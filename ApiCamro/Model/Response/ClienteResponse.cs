﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model.Response
{
    public class ClienteResponse
    {
        public int entidadId { get; set; }
        public string nombre { get; set; }
        public string nombreComercial { get; set; }
        public string rfc { get; set; }
        public string direccion { get; set; }
        public int entidadTipo { get; set; }
        public int entidadIdPadre { get; set; }
        public double puntosSaldo { get; set; }
        public string codigo { get; set; }
        public string razonSocial { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string telefonoPrincipal { get; set; }
        public string telefonoMovil { get; set; }
        public string correo { get; set; }
        public string sitio_web { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string clasificacion { get; set; }
        public int paisId { get; set; }
        public string codigoPostal { get; set; }
        public string calle { get; set; }
        public string numeroExterior { get; set; }
        public string numeroInterior { get; set; }
        public string colonia { get; set; }
        public int ciudadId { get; set; }
        public string ciudadNombre { get; set; }
        public int asentamientoId { get; set; }
        public string asentamiento { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public bool esSujetoCredito { get; set; }
        public double limiteCredito { get; set; }
        public int diasCredito { get; set; }
        public bool permitirFacturasVencidas { get; set; }
        public double descuentoProntoPago { get; set; }
        public int diasProntoPago { get; set; }
        public double limiteDescuento { get; set; }
        public int vendedorId { get; set; }
        public int listaPrecioId { get; set; }
        public int precio { get; set; }
    }
}
