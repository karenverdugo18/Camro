﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model.Request
{
    public class CotizadorReq
    {
        public string entidadId { get; set; }
        public List<long> conceptoIdList { get; set; }


    }
}
