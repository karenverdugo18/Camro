﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model
{
    public class ProductoPrecio
    {
        public int consecutivo { get; set; }
        public int conceptoId { get; set; }
        public string nombre { get; set; }
        public string moneda { get; set; }
        public string codigo { get; set; }
        public int umId { get; set; }
        public string unidad { get; set; }
        public double costo_base { get; set; }
        public double precio1 { get; set; }
        public double precio2 { get; set; }
        public double precio3 { get; set; }
        public double precio4 { get; set; }
    }
}
