﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model
{
    public class Producto
    {
        public int conceptoId { get; set; }
        public string conceptoNombre { get; set; }
        public int clasificacionId { get; set; }
        public string clasificacionNombre { get; set; }
        public int marcaId { get; set; }
        public string marca { get; set; }
        public string codigo_barras { get; set; }
        public string nombre_corto { get; set; }
        public double precio { get; set; }
        public int impuestoId3 { get; set; }
        public double impuestoId3_valor { get; set; }
        public double impuestoId3_valor_neto { get; set; }
        public int impuestoId1 { get; set; }
        public double impuestoId1_valor { get; set; }
        public double impuestoId1_valor_neto { get; set; }
        public int permiteDescuento { get; set; }
        public double descuento { get; set; }
        public double descuentoPorcentaje { get; set; }
        public int permiteCambiarPrecio { get; set; }
        public int unidadId { get; set; }
        public string unidadMedida { get; set; }
        public double peso { get; set; }
        public double alto { get; set; }
        public double ancho { get; set; }
        public double largo { get; set; }
        public string costoIdentificado { get; set; }
        public int costoIdentificadoTipo { get; set; }
        public double existencia { get; set; }
    }
}
