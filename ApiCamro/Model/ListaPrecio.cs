﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model
{
    public class ListaPrecio
    {
        public int listaId { get; set; }
        public int monedaId { get; set; }
        public string nombre { get; set; }
        public bool activo { get; set; }
        public int sucursalId { get; set; }
        public string sucursal { get; set; }
        public string comentario { get; set; }
        public bool restringirPrecioVenta { get; set; }
        public bool restringirDescVenta { get; set; }
        public string vencimiento { get; set; }
        public int configuracionPrecioId { get; set; }
        public string configuracionPrecio { get; set; }
        public List<ProductoPrecio> articulos { get; set; }
    }
}
