﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCamro.Model
{
    public class TokenRequest
    {
        public string token { get; set; }
        public string type { get; set; }
    }
}
