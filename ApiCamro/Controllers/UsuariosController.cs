﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ApiCamro.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ApiCamro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : Controller
    {


        [Route("getTokenCrol")]
        [HttpGet]
        public async Task<TokenRequest> getTokenCrol(IConfiguration configuration)
        {

            string url = @"http://afcrolapijwt.azurewebsites.net/api/Auth?code=MVNADGLToxXVbxlcEARFqyjyUF4NNBQmrNE1mV7916vDbrvoQtdATA==";
            try
            {
                using (var client = new HttpClient())
                {
                    var obj = new {
                                 APIKey = configuration["TokenCrol"]// "D8ED7E1B-A910-4BB1-A78E-E0C9F1640269"
                                };
                    client.BaseAddress = new Uri("http://afcrolapijwt.azurewebsites.net");

                    string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                    var result = client.PostAsync("api/Auth?code=MVNADGLToxXVbxlcEARFqyjyUF4NNBQmrNE1mV7916vDbrvoQtdATA==", stringContent);

                    var r = result.Result;
                    var responseJson = await result.Result.Content.ReadAsStringAsync();
                    JObject jreq = JObject.Parse(responseJson);

                    // Copy to a static Album instance
                    TokenRequest req = jreq.ToObject<TokenRequest>();
                    return  (req);

                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}