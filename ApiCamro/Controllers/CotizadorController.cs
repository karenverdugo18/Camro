﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ApiCamro.Model;
using ApiCamro.Model.Request;
using ApiCamro.Model.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ApiCamro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotizadorController : Controller
    {
        private readonly IConfiguration Configuration;

        public CotizadorController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        [Route("CotizadorResult")]
        [HttpPost]
        public async Task<ActionResult> CotizadorResult([FromBody]CotizadorReq cotreq)
        {
            UsuariosController crol = new UsuariosController();
            var req = crol.getTokenCrol(Configuration);
            string urlBase = @"http://afcrolapijwt.azurewebsites.net";
            string reqS = "";
            var tokenInfo = new TokenRequest
            {
                token = req.Result.token,
                type = req.Result.type
            };

            List<ContactoResponse >con = new List<ContactoResponse>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenInfo.type, tokenInfo.token);

                var obj = new
                {
                    organizacionId = 6057,
                    entidadId = 0,
                    busqueda = ""
                };
                client.BaseAddress = new Uri(urlBase);

                string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = client.PostAsync("api/Function/_APISmart_cmb_listadoContactos", stringContent);

                var r = result.Result;
                var responseJson = await result.Result.Content.ReadAsStringAsync();
                responseJson = responseJson.Substring(1);
                responseJson = responseJson.Substring(0, responseJson.Length - 1);
                try
                {
                    con = JsonConvert.DeserializeObject<List<ContactoResponse>>(responseJson);

                    //  return Json(products);

                }
                catch (Exception e)
                {
                    string x = "";
                }



            }
            List<Producto> products = new List<Producto>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenInfo.type, tokenInfo.token);

                var obj = new
                {
                    organizacionId = 6057,
                    clasificacionId = 0,
                    sucursalId = 0,
                    almacenId = 0,
                    divisionId = 0,
                    listaId = 0,
                    precioId = 1,
                    conceptoId = 0
                };
                client.BaseAddress = new Uri(urlBase);

                string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = client.PostAsync("api/Function/_APISmart_cmb_productos", stringContent);

                var r = result.Result;
                var responseJson = await result.Result.Content.ReadAsStringAsync();
                responseJson = responseJson.Substring(1);
                responseJson = responseJson.Substring(0, responseJson.Length - 1);
                try
                {
                    products = JsonConvert.DeserializeObject<List<Producto>>(responseJson);

                    //  return Json(products);

                }
                catch (Exception e)
                {
                    string x = "";
                }



            }

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenInfo.token);

                var obj = new
                {
                    organizacionId = 6057,
                    usuarioId = -1
                };
                client.BaseAddress = new Uri(urlBase);

                string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = client.PostAsync("api/Function/_APISmart_prc_listaprecios", stringContent);

                var r = result.Result;
                var responseJson = await result.Result.Content.ReadAsStringAsync();
                responseJson = responseJson.Substring(2);
                responseJson = responseJson.Substring(0, responseJson.Length - 2);
                try
                {
                    var Precio = JsonConvert.DeserializeObject<List<ListaPrecio>>(responseJson);
                    var ListaPrecioCliente = Precio.Where(x => x.listaId == con[0].listaPrecioId).FirstOrDefault();
                    var productosPrecio = from p in products
                                          join prec in ListaPrecioCliente.articulos on p.conceptoId equals prec.conceptoId
                                          select new
                                          {
                                              conceptoId = p.conceptoId,
                                              conceptoNombre = p.conceptoNombre,
                                              clasificacionId = p.clasificacionId,
                                              clasificacionNombre = p.clasificacionNombre,
                                              marcaId = p.marcaId,
                                              marca = p.marca,
                                              precio = (products[0].precio.ToString() == "1" ? prec.precio1 : products[0].precio.ToString() == "2" ? prec.precio2 : products[0].precio.ToString() == "3" ? prec.precio3 : products[0].precio.ToString() == "4" ? prec.precio4 : prec.precio1),
                                              existencia = p.existencia

                                          };

                    return Json(productosPrecio.ToList());

                }
                catch (Exception e)
                {
                    string x = "";
                }



            }
            return Json(products);

        }
    }
}