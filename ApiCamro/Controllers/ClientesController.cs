﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ApiCamro.Model;
using ApiCamro.Model.Request;
using ApiCamro.Model.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ApiCamro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : Controller
    {
        private readonly IConfiguration Configuration;

        public ClientesController(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        [Route("getClientes")]
        [HttpGet]
        public async Task<ActionResult> getProducts(string nombreCliente)
        {
            UsuariosController crol = new UsuariosController();
            var req = crol.getTokenCrol(Configuration);
            string urlBase = @"http://afcrolapijwt.azurewebsites.net";
            string reqS = "";
            var tokenInfo = new TokenRequest
            {
                token = req.Result.token,
                type = req.Result.type
            };
            List<ClienteResponse> clients = new List<ClienteResponse>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenInfo.type, tokenInfo.token);

                var obj = new
                {
                    organizacionId = 6057,
                    entidadId = 0,
                    busqueda =nombreCliente==null?"":nombreCliente
                };
                client.BaseAddress = new Uri(urlBase);

                string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = client.PostAsync("api/Function/_APISmart_cmb_listadoContactos", stringContent);

                var r = result.Result;
                
                var responseJson = await result.Result.Content.ReadAsStringAsync();
                responseJson = responseJson.Substring(1);
                responseJson = responseJson.Substring(0, responseJson.Length - 1);
                try
                {
                    clients = JsonConvert.DeserializeObject<List<ClienteResponse>>(responseJson);

                    //  return Json(products);

                }
                catch (Exception e)
                {
                    string x= "";
                }



            }
           
            return Json(clients);

        }
    }
}