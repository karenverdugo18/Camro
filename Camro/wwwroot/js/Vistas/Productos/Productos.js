﻿function buscarProducto() {
    mostrarCargando();

    var objLineas = [];
    var objSublineas = [];
    var objSubSublineas = [];
    var objYear = [];
    var objModel = [];
    var objMarca = [];




    $(".chModelo:checked").map(function (i, item) {
        objModel.push($(item).data("propid"))
  })

  $(".chYear:checked").map(function (i, item) {
        objYear.push($(item).data("propid"))
  })

    $(".chMarca:checked").map(function (i, item) {
        objMarca.push($(item).data("propid"))
    })

    $(".chLinea:checked").map(function (i, item) {
        objLineas.push($(item).data("propid"))
    });

    $(".chSublinea:checked").map(function (i, item) {
        objSublineas.push($(item).data("propid"))
    })
    $(".chSubsublinea:checked").map(function (i, item) {
        objSubSublineas.push($(item).data("propid"))
    })


  


    $.ajax({
        url: urlApi + 'Products/SearchProducts',
        crossDomain: true,
        dataType: "json", 
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            Marca: objMarca,
            Modelo: objModel,
            lines: objLineas,
            subLines: objSublineas,
            subSubLines: objSubSublineas,
        }),
        success: function (respuesta) {

            var html = "";

            respuesta.map(function (item, i) {
                $("#contentProducts").empty();

                html += `
                    <div class="col-lg-4 col-sm-6" id="capa-variable">
                    <div class="product-item">
                        <div class="pi-pic">
                            <img  src="../img/INICIO/prod-1.png"  alt="">
                                <!-- <div class="sale pp-sale">Sale</div> -->
                                    <div class="icon">
                                    <i class="fas fa-search-plus"></i>
                                </div>
                                <ul style="width:100%">
                                    <li class="w-icon z-depth-1"><a href="./InfoProducto"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li class="quick-view z-depth-1"> <a href="#" class="btn-dark blanco btn-block ">Agregar Al Carrito</a></li>

                                </ul>
                                </div>
                            <div class="pi-text">

                                <a href="#">
                                    <h4 class="negrita">`+ item.shortDescription + `</h4>
                                </a>
                                <div class="product-price naranjita">
                                    $`+ item.arPrice + `

                                    </div>
                            </div>
                        </div>
                        <hr />
                </div>`
            });

            $("#contentProducts").append(html);

            ocultarCargando();

        },
        error: function (err,x,x) {
            console.log("No se ha podido obtener la información");
                ocultarCargando();
        }
    });
}

//MARCA//
function cargarMarca() {
    mostrarCargando();

    $.ajax({
        url: urlApi + 'Manufacturers',
        crossDomain: true,
        dataType: "json",
        success: function (respuesta) {

            var html = "";

            respuesta.map(function (item, i) {
                $("#divMarca").empty();

                html += `
                          <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="shadow-box-example z-depth-2 custom-control-input  chMarca" onclick="cargarModelo()" data-propid="`+ item.id + `" id="Marca` + item.id + `">
                                    <label class="custom-control-label font negrita chMarca" for="Marca`+ item.id + `" data-propId="` + item.id + `">` + item.name + `</label>
                                </div><br/>

                        `
            });

            $("#divMarca").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {

            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}

//MARCA//

//MODELO//

function cargarModelo() {
    mostrarCargando();
    var objMarca = [];
    $(".chMarca:checked").map(function (i, item) {

        objMarca.push($(item).data("propid"))

    });
    $.ajax({
        url: urlApi + 'models/SearchModel',
        crossDomain: true,
        dataType: "json",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
           Marca : objMarca
        }),
        success: function (respuesta) {

            var html = "";
            $("#divModelo").empty();

            respuesta.map(function (item, i) {

                html += `
                          <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="shadow-box-example z-depth-2 custom-control-input  chModelo" onclick="" data-propid="`+ item.id + `" id="Modelo` + item.id + `">
                                    <label class="custom-control-label font negrita chModelo" for="Modelo`+ item.id + `" data-propId="` + item.id + `">` + item.name + `</label>
                                </div><br/>

                        `
            });

            $("#divModelo").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {
            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}

//Modelo//



//Lineas//
function cargarLineas() {
    mostrarCargando();

    $.ajax({
        url: urlApi + 'Lines',
        crossDomain: true,
        dataType: "json",        
        success: function (respuesta) {

            var html = "";
                $("#divLineas").empty();

            respuesta.map(function (item, i) {

                html += `
                          <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="shadow-box-example z-depth-2 custom-control-input  chLinea" onclick="cargarSublineas()" data-propid="`+ item.id +`" id="Linea`+ item.id +`">
                                    <label class="custom-control-label font negrita chLinea" for="Linea`+item.id+`" data-propId="`+item.id+`">`+item.name+`</label>
                                </div><br/>

                        `
            });

            $("#divLineas").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {

            console.log("No se ha podido obtener la información");
                ocultarCargando();
        }
    });
}

//Lineas//

//SubLineas//

function cargarSublineas() {
    mostrarCargando();
    var objLineas = [];
    $(".chLinea:checked").map(function (i, item) {

        objLineas.push($(item).data("propid"))

    });
    $.ajax({
        url: urlApi + 'Sublines/SearchSublines',
        crossDomain: true,
        dataType: "json",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            lines: objLineas
        }),
        success: function (respuesta) {

            var html = "";
            $("#divSublineas").empty();

            respuesta.map(function (item, i) {

                html += `
                          <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="shadow-box-example z-depth-2 custom-control-input  chSublinea" onclick=" cargarSubsublineas()" data-propid="`+ item.id + `" id="Sublinea` + item.id + `">
                                    <label class="custom-control-label font negrita chSublinea" for="Sublinea`+ item.id + `" data-propId="` + item.id + `">` + item.name + `</label>
                                </div><br/>

                        `
            });

            $("#divSublineas").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {
            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}

//SubLineas//


//SubSubLineas//

function cargarSubsublineas() {
    mostrarCargando();
    var objSublineas = [];
    $(".chSublinea:checked").map(function (i, item) {

        objSublineas.push($(item).data("propid"))
        

    });
    
    $.ajax({
        url: urlApi + 'Subsublines/SearchSubsublines',
        crossDomain: true,
        dataType: "json",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            Sublines: objSublineas
        }),
        success: function (respuesta) {

            var html = "";

            respuesta.map(function (item, i) {
                $("#divSubsublineas").empty();

                html += `
                          <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="shadow-box-example z-depth-2 custom-control-input  chSubsublinea" onclick="" data-propid="`+ item.id + `" id="Subsublinea` + item.id + `">
                                    <label class="custom-control-label font negrita chSubsublinea" for="Subsublinea`+ item.id + `" data-propId="` + item.id + `">` + item.name + `</label>
                                </div><br/>

                        `
            });

            $("#divSubsublineas").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {
            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}

//SubLineas//







var modview = 1;

function CambiarEstilo() {
    debugger
    if (modview == 1) {
        $(".col-lg-4").addClass("col-lg-12");

        modview = 2;
    }
    else {
        $(".col-lg-4").removeClass("col-lg-12");
        modview = 1;
    }
}

$(function () {
    cargarMarca(); 
    cargarLineas();
   

    $('.chLinea').click(function () {
        cargarSublineas();
    });

})