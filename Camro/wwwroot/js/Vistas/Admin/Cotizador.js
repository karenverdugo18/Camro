﻿function buscarProducto() {
    mostrarCargando();

    var objLineas = [];
    var objSublineas = [];
    var objSubSublineas = [];
    var objYear = [];
    var objModel = [];
    var objMarca = [];




    $(".chModelo:checked").map(function (i, item) {
        objModel.push($(item).data("propid"))
  })

  $(".chYear:checked").map(function (i, item) {
        objYear.push($(item).data("propid"))
  })

    $(".chMarca:checked").map(function (i, item) {
        objMarca.push($(item).data("propid"))
    })

    $(".chLinea:checked").map(function (i, item) {
        objLineas.push($(item).data("propid"))
    });

    $(".chSublinea:checked").map(function (i, item) {
        objSublineas.push($(item).data("propid"))
    })
    $(".chSubsublinea:checked").map(function (i, item) {
        objSubSublineas.push($(item).data("propid"))
    })


  


    $.ajax({
        url: urlApi + 'Products/SearchProducts',
        crossDomain: true,
        dataType: "json", 
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            Marca: objMarca,
            Modelo: objModel,
            lines: objLineas,
            subLines: objSublineas,
            subSubLines: objSubSublineas,
        }),
        success: function (respuesta) {

            var html = "";

            respuesta.map(function (item, i) {
                $("#contentProducts").empty();

                html += `
                    <div class="col-lg-4 col-sm-6" id="capa-variable">
                    <div class="product-item">
                        <div class="pi-pic">
                            <img  src="../img/INICIO/prod-1.png"  alt="">
                                <!-- <div class="sale pp-sale">Sale</div> -->
                                    <div class="icon">
                                    <i class="fas fa-search-plus"></i>
                                </div>
                                <ul style="width:100%">
                                    <li class="w-icon z-depth-1"><a href="./InfoProducto"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li class="quick-view z-depth-1"> <a href="#" class="btn-dark blanco btn-block ">Agregar Al Carrito</a></li>

                                </ul>
                                </div>
                            <div class="pi-text">

                                <a href="#">
                                    <h4 class="negrita">`+ item.shortDescription + `</h4>
                                </a>
                                <div class="product-price naranjita">
                                    $`+ item.arPrice + `

                                    </div>
                            </div>
                        </div>
                        <hr />
                </div>`
            });

            $("#contentProducts").append(html);

            ocultarCargando();

        },
        error: function (err,x,x) {
            console.log("No se ha podido obtener la información");
                ocultarCargando();
        }
    });
}

//MARCA//
function cargarClientes() {
    mostrarCargando();
    var cmbText = $("#cmbClientes").val();
    cmbText = cmbText == null ? "" : cmbText;
    $.ajax({
        url: urlApi + 'Clientes/getClientes?nombreCliente=' + cmbText,
        crossDomain: true,
        dataType: "json",
        success: function (respuesta) {

           var sel = $("#cmbCliente");
           sel.empty();

            respuesta.map(function (item, i) {
                    sel.append('<option value="' + item.entidadId + '">' + item.nombre + '</option>');
            });

            sel.val(localStorage.getItem('conceptoId'));

            sel.selectpicker('refresh');


            ocultarCargando();

        },
        error: function (err, x, x) {

            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}
 
   

$(function () {
    $('#cmbCliente').selectpicker();
    $('select').on('change', function (e) {
        localStorage.setItem('conceptoId', $('#cmbCliente').val());
        localStorage.setItem('nombre', $('#cmbCliente option:selected').text());
        $("#lblUsuario").empty();

        $("#lblUsuario").append("Cliente: " + localStorage.getItem('nombre'));
    });
    cargarClientes(); 

})