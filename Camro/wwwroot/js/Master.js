﻿
var objLineas2 = [];

$(".Linea").map(function (i, item) {
    objLineas2.push($(item).data("propid"))
});

//Lineas//
function cargarLineas2() {

    mostrarCargando();

    $.ajax({
        url: urlApi + 'Lines',
        crossDomain: true,
        dataType: "json",
        success: function (respuesta) {

            var html = "";
            $("#Lineas").empty();

            respuesta.map(function (item, i) {

                html += `

                             <a class="dropdown-item"  id="Linea` + item.id + `" href="./Productos/productos">  <label class=" font negrita Linea" for="Linea` + item.id + `" data-propId="` + item.id + `">` + item.name + `</label> </a>
                 
                        `
            });

            $("#Lineas").append(html);

            ocultarCargando();

        },
        error: function (err, x, x) {

            console.log("No se ha podido obtener la información");
            ocultarCargando();
        }
    });
}

//Lineas//
$(function () {
   
    cargarLineas2();


})